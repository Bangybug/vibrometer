/**
 * BMA020 Accelerometer test. 
 * 
 * Wires connection:
 * 
 * Vcc_IN   - 5V
 * GND      - GND
 * SCL      - A5
 * SDA      - A4
 * 
 * Serial commands:
 * AINIT resolutionIndex bandwidthIndex
 * AREAD channelFlags
 * 
 * author Dmitry Pryadkin, drpadawan@ya.ru
 */
#include <Wire.h> 
#include "SerialCommand.h"

//address of the accelerometer (may be different on different boards)
#define BMA020_ADDRESS 0x38

// some registers (taken from datasheet Chapter 7. Global memory map)
#define BMA020_RANGE_BW 0X14  // bits 5,6,7 must be kept unchanged
#define BMA020_DATA 0x02
#define BMA020_CTL 0x15

// Chapter "3.1.2 Range" of the datasheet defines 3 various measurement ranges in terms of +/-g: 2, 4, 8
// The narrower the range, the higher precision you would get.
const int resolutionsCount = 3 - 1;

// Chapter "3.1.3 Bandwidth" defines 7 various bandwidths in terms of Hz: 25, 50, 100, 190, 375, 750, 1500
const int bandwidthsCount = 7 - 1;

unsigned long lastAccelerometerReadMicros;

const char *ERR = "ERR";
const char *OK = "OK";

bool accelerometerInitialized = false;
bool resetElapsedTime = false;
byte accelerometerChannelReadFlags = 0x0;


SerialCommand SCmd;


void setup() 
{
    // not all arduinos will handle 115200
    Serial.begin(115200); 

    SCmd.addCommand("AINIT", commandAccelerometerInit );
    SCmd.addCommand("AREAD", commandAccelerometerRead );
    SCmd.addDefaultHandler( commandUnknown );

    Wire.begin(); 
} 


void loop() 
{ 
    SCmd.readSerial();

    if (accelerometerInitialized && accelerometerChannelReadFlags)
        AccelerometerReadAndSend(); 
    
}



void AccelerometerInit(int resolutionIndex, int bandwidthIndex)
{ 
    resolutionIndex = constrain(resolutionIndex, 0, resolutionsCount);
    bandwidthIndex = constrain(bandwidthIndex, 0, bandwidthsCount);
    
    byte temp[1];
    byte temp1;

    // choose bandwidth and range    
    readFrom(BMA020_ADDRESS, BMA020_RANGE_BW,1,temp);
    temp1= (bandwidthIndex & 0x7) | ((resolutionIndex & 0x18) << 3) | (temp[0] & 0xe0);
    writeTo(BMA020_ADDRESS, BMA020_RANGE_BW, temp1);   
    
    // Chapter "3.1.6 Shadow_dis " says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
    // explains that the locking is done if shadow_dis=0. We need this locking for consistent LSB and MSB byte reads.
    readFrom(BMA020_ADDRESS, BMA020_CTL, 1, temp);
    temp1= (temp[0] & 0xf7);
    writeTo(BMA020_ADDRESS,BMA020_CTL, temp1);
    
    accelerometerInitialized = true;
}



void AccelerometerSetOffset( byte channelIndex, byte offsetLSB, byte offsetMSB )
{
    
}


void convertAndPrintAccel(byte* acc)
{
    int r = ( (acc[0] & 0xc0) | (acc[1] << 8) ) >> 6; 
    float fx = r / 256.0f;
    Serial.print(fx);
    Serial.print("g"); 
    Serial.print(" ");
}

void AccelerometerReadAndSend()
{ 
    byte result[2];

    // X
    if (accelerometerChannelReadFlags & 0x1)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA, 2, result);
        convertAndPrintAccel( result ); 
    }
    // Y
    if (accelerometerChannelReadFlags & 0x2)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA+2, 2, result);
        convertAndPrintAccel( result ); 
    }
    // Z
    if (accelerometerChannelReadFlags & 0x4)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA+4, 2, result);
        convertAndPrintAccel( result ); 
    }
    Serial.println();

    delay(1000);
}

//Writes val to address register on ACC
void writeTo(int DEVICE, byte address, byte val) 
{
    Wire.beginTransmission(DEVICE);   //start transmission to ACC
    Wire.write(address);               //send register address
    Wire.write(val);                   //send value to write
    Wire.endTransmission();           //end trnsmisson
}

// Reads num bytes starting from address register in to buff array. 
// This is also used to read output channels for X,Y,Z. It can read all at once, or individual channels.
// Chapter "7.12.2 acc_x, acc_y, acc_z" says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
// Chapter "7.7.6 showdow_dis" explains that the locking is done if shadow_dis=0.
void readFrom(int DEVICE, byte address , int num ,byte *buff)
{
    Wire.beginTransmission(DEVICE);
    Wire.write(address);
    Wire.endTransmission();
 
    Wire.beginTransmission(DEVICE);
    Wire.requestFrom(DEVICE,num); 
 
    int i=0;
    while(Wire.available())
    {
        buff[i] = Wire.read();
        i++;
    }
    Wire.endTransmission();
}


bool decodeIntArgs( int num, int buff[] )
{
    bool err = false;
    for (int i=0; i<num; ++i)
    {
        char *arg = SCmd.next(); 
        if (arg) 
        {
            buff[i] = atoi(arg);
        }
        else
        {
            err = true;
            break;
        }
    }
    return !err;
}

void printArgs(int num, int buff[])
{
    for (int i=0; i<num; ++i)
    {
        if (i > 0)
            Serial.print(", ");
        Serial.print( buff[i] );
    }
    Serial.println();
}

void commandAccelerometerInit()    
{
    int args[2];
    if (decodeIntArgs(2, args))
    {
        Serial.print(OK);
        
        AccelerometerInit( args[0], args[1] );
        delay(20);

        // read back and send following registers
        // resolution and range
        args[0] = 0;
        readFrom(BMA020_ADDRESS, BMA020_RANGE_BW,1, (byte*)args);
        printArgs( 1, args );
    }
    else
    {
        Serial.println(ERR);
    }
}

void commandUnknown()
{
    Serial.println(ERR); 
}


void commandAccelerometerRead()
{
    int args[1];
    if (decodeIntArgs(1, args))
    {
        printArgs( 1, args );
        if (args[0] < 0 || args[0] > 7)
            Serial.println(ERR);
        else
        {
            resetElapsedTime = true;
            accelerometerChannelReadFlags = (byte)args[0];
            Serial.print(OK);
            printArgs( 1, args );
        }
    }
    else
    {
        Serial.println(ERR);
    }
}

