/**
 * BMA180 Accelerometer serial data streamer. 
 * author Dmitry Pryadkin, drpadawan@ya.ru
 * https://bitbucket.org/Bangybug/vibrometer
 * This software developed for Arduino Uno (should work with other arduinos). 
 * 
 * The goal is to acquire data via i2c as fast as possible and transmit it to the PC for processing. 
 * There are limits, the BMA180 provides various sample rates up to 1200 Hz.  The Arduino i2c speed is limited, as well as the Serial communications has its limits.
 * For the purposes of speed, Serial communications should be in binary form, the COM monitor won't show anything menaningful.  Also we will want to request lesser data from the accelerometer, for example only Z acceleration.
 * 
 * This software does not write anything to the EEPROM.
 * 
 * Communication estimations:
 * 
 * The Arduinos I2c speed is 100kHz. I have not attempted to increase it. Each byte is read or written using 9 cycles (minimum). 
 * Updating a single channel involves transfer of 3 bytes, that is 27 cycles, and at least 10 more cycles (I didn't check) for the service. 
 * So we can update single channel data at 100 kHz / 37 = 2.7 kHz, which is quite tight (slow) even for single channel update.
 * 
 * The Serial transfer to PC is asynchronous. That means, we don't know how much it affects the update frequency. It shouldn't affect it very much though. 
 * At 115200 it will transfer an update quite fast, roughly at 6 kHz.
 * 
 * Having said all that, it is hardly possible to know the resulting sample rate, and likely it will not be fixed. But the performance shouldn't be that bad really. 
 * So we would transfer the timestamp along with each update. A timestamp is an elapsed time in microseconds. On arduino 16Mhz the resolution is 4 microseconds per LSB.
 * 
 * Wires connection:
 * 
 * Vcc_IN   - 5V
 * GND      - GND
 * SCL      - A5
 * SDA      - A4
 * 
 * Serial commands:
 * AINIT resolutionIndex bandwidthIndex
 * AGAIN channelIndex gainIndex
 * AREAD channelFlags
 * 
 */
#include <Wire.h> 
#include "SerialCommand.h"

//address of the accelerometer (may be different on different boards)
#define BMA180_ADDRESS 0x38

// some registers (taken from datasheet Chapter 7. Global memory map)
#define BMA180_RESET 0x10
#define BMA180_PWR 0x0D
#define BMA180_BW 0X20
#define BMA180_RANGE 0X35
#define BMA180_DATA 0x02
#define BMA180_GAINX 0x32

// Chapter "7.7.1 Range" of the datasheet defines 7 various measurement ranges in terms of +/-g: 1, 1.5, 2, 3, 4, 8, 16
// The narrower the range, the higher precision you would get.
const int resolutionsCount = 7 - 1;

// Chapter "7.7.2 bw" defines 10 various bandwidths in terms of Hz: 10, 20, 40, 75, 150, 300, 600, 1200, 1, and bandpass 0.2-300
const int bandwidthsCount = 10 - 1;

// Chapter "7.9.1 Gain trimming (sensitivity trimming)" explains how to adjust sensitivity per individual x/y/z channels
// This involves writing special 'binary offset' coded numbers to the channel gain registers. These numbers are 7-bit wide, thus max limited to 127
const int gainsCount = 127;

unsigned long lastAccelerometerReadMicros;

const char *ERR = "ERR";
const char *OK = "OK";

bool accelerometerInitialized = false;
bool resetElapsedTime = false;
byte accelerometerChannelReadFlags = 0x0;


SerialCommand SCmd;


void setup() 
{
    // not all arduinos will handle 115200
    Serial.begin(115200); 

    SCmd.addCommand("AINIT", commandAccelerometerInit );
    SCmd.addCommand("AGAIN", commandAccelerometerSetGain );
    SCmd.addCommand("AREAD", commandAccelerometerRead );
    SCmd.addDefaultHandler( commandUnknown );

    Wire.begin(); 
} 


void loop() 
{ 
    SCmd.readSerial();

    if (accelerometerInitialized && accelerometerChannelReadFlags)
        AccelerometerReadAndSend(); 
    
}



void AccelerometerInit(int resolutionIndex, int bandwidthIndex)
{ 
    resolutionIndex = constrain(resolutionIndex, 0, resolutionsCount);
    bandwidthIndex = constrain(bandwidthIndex, 0, bandwidthsCount);
    
    byte temp[1];
    byte temp1;

    // Chapter "7.10.6 soft_reset" says that this command is identical to power-on-reset, control, status and image registers are restored from EEPROM
    // probably we don't need this here
    writeTo(BMA180_ADDRESS,BMA180_RESET,0xB6);

    // this sets ee_w enabling writing to EEPROM and Image registers. We write to image registers only.
    writeTo(BMA180_ADDRESS,BMA180_PWR,0x10);
    
    // choose bandwidth
    readFrom(BMA180_ADDRESS, BMA180_BW,1,temp);
    temp1= (temp[0] & 0x0F) | (bandwidthIndex << 4);
    writeTo(BMA180_ADDRESS, BMA180_BW, temp1);   
    
    // choose range
    readFrom(BMA180_ADDRESS, BMA180_RANGE, 1 ,temp);  
    temp1= (temp[0] & 0xF1) | (resolutionIndex << 1);
    writeTo(BMA180_ADDRESS,BMA180_RANGE,temp1);

    // Chapter "7.12.2 acc_x, acc_y, acc_z" says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
    // Chapter "7.7.6 showdow_dis" explains that the locking is done if shadow_dis=0. We need this locking for consistent LSB and MSB byte reads.
    readFrom(BMA180_ADDRESS, BMA180_GAINX+1, 1, temp);
    temp1= (temp[0] & 0xfe);
    writeTo(BMA180_ADDRESS, BMA180_GAINX+1, temp1);
    
    accelerometerInitialized = true;
}


void AccelerometerSetGain( byte channelIndex, byte gainValue )
{
    gainValue = constrain( gainValue, 0, gainsCount );
    channelIndex = constrain( channelIndex, 0, 2 );

    byte temp[1];
    byte temp1;
    
    readFrom(BMA180_ADDRESS, BMA180_GAINX+channelIndex, 1, temp);
    temp1= (temp[0] & 0x1) | (gainValue << 1);
    writeTo(BMA180_ADDRESS, BMA180_GAINX+channelIndex, temp1);
}


void AccelerometerSetOffset( byte channelIndex, byte offsetLSB, byte offsetMSB )
{
    
}


void AccelerometerReadAndSend()
{ 
    long elapsedTime;
    unsigned long now;
    byte result[2];

    // X
    if (accelerometerChannelReadFlags & 0x1)
    {
        readFrom(BMA180_ADDRESS, BMA180_DATA, 2, result);
        Serial.write(result, 2);
    }
    // Y
    if (accelerometerChannelReadFlags & 0x2)
    {
        readFrom(BMA180_ADDRESS, BMA180_DATA+2, 2, result);
        Serial.write(result, 2);
    }
    // Z
    if (accelerometerChannelReadFlags & 0x4)
    {
        readFrom(BMA180_ADDRESS, BMA180_DATA+4, 2, result);
        Serial.write(result, 2);
    }
    now = micros();
    if (resetElapsedTime)
    {
        resetElapsedTime = false;
        lastAccelerometerReadMicros = now;
        elapsedTime = 0;
    }
    else
    {
        // this cast deals with possible overflow (aka rollover), elapsedTime should be positive if the "lastAccelerometerReadMicros" is within single overflow range from "now"
        elapsedTime = (long)(now - lastAccelerometerReadMicros);
        lastAccelerometerReadMicros = now;
    }
    Serial.write((byte*) &elapsedTime, 4);

//
//    Serial.println();
//    for (int i=0; i<2; i++)
//        Serial.println(result[i]);
//    Serial.println();
     //int x= (( result[0] | result[1]<<8)>>2)+offx ;
}

//Writes val to address register on ACC
void writeTo(int DEVICE, byte address, byte val) 
{
    Wire.beginTransmission(DEVICE);   //start transmission to ACC
    Wire.write(address);               //send register address
    Wire.write(val);                   //send value to write
    Wire.endTransmission();           //end trnsmisson
}

// Reads num bytes starting from address register in to buff array. 
// This is also used to read output channels for X,Y,Z. It can read all at once, or individual channels.
// Chapter "7.12.2 acc_x, acc_y, acc_z" says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
// Chapter "7.7.6 showdow_dis" explains that the locking is done if shadow_dis=0.
void readFrom(int DEVICE, byte address , int num ,byte *buff)
{
    Wire.beginTransmission(DEVICE);
    Wire.write(address);
    Wire.endTransmission();
 
    Wire.beginTransmission(DEVICE);
    Wire.requestFrom(DEVICE,num); 
 
    int i=0;
    while(Wire.available())
    {
        buff[i] = Wire.read();
        i++;
    }
    Wire.endTransmission();
}


bool decodeIntArgs( int num, int buff[] )
{
    bool err = false;
    for (int i=0; i<num; ++i)
    {
        char *arg = SCmd.next(); 
        if (arg) 
        {
            buff[i] = atoi(arg);
        }
        else
        {
            err = true;
            break;
        }
    }
    return !err;
}

void printArgs(int num, int buff[])
{
    for (int i=0; i<num; ++i)
    {
        if (i > 0)
            Serial.print(", ");
        Serial.print( buff[i] );
    }
    Serial.println();
}

void commandAccelerometerInit()    
{
    int args[5];
    if (decodeIntArgs(2, args))
    {
        Serial.print(OK);
        
        AccelerometerInit( args[0], args[1] );
        delay(20);

        // read back and send following registers
        memset(args, 0, sizeof(int)*5);

        // resolution and range
        readFrom(BMA180_ADDRESS, BMA180_RANGE, 1, (byte*)args);  
        readFrom(BMA180_ADDRESS, BMA180_BW, 1, (byte*)(args+1));  
        // gain
        readFrom(BMA180_ADDRESS, BMA180_GAINX+0, 1, (byte*)(args+2));
        readFrom(BMA180_ADDRESS, BMA180_GAINX+1, 1, (byte*)(args+3));
        readFrom(BMA180_ADDRESS, BMA180_GAINX+2, 1, (byte*)(args+4));

        printArgs( 5, args );
    }
    else
    {
        Serial.println(ERR);
    }
}

void commandUnknown()
{
    Serial.println(ERR); 
}


void commandAccelerometerSetGain()
{
    int args[2];
    if (decodeIntArgs(2, args))
    {
        Serial.print(OK);
        printArgs( 2, args );
        AccelerometerSetGain( args[0], args[1] );
    }
    else
    {
        Serial.println(ERR);
    }    
}

void commandAccelerometerRead()
{
    int args[1];
    if (decodeIntArgs(1, args))
    {
        printArgs( 1, args );
        if (args[0] < 0 || args[0] > 7)
            Serial.println(ERR);
        else
        {
            resetElapsedTime = true;
            accelerometerChannelReadFlags = (byte)args[0];
            Serial.print(OK);
            printArgs( 1, args );
        }
    }
    else
    {
        Serial.println(ERR);
    }
}

