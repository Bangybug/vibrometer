/* BMA020 Accelerometer serial data streamer. 
 * author Dmitry Pryadkin, drpadawan@ya.ru
 * https://bitbucket.org/Bangybug/vibrometer
 * 
 * This software developed for Arduino Uno (should work with other arduinos). 
 * 
 * The goal is to acquire data via i2c as fast as possible and transmit it to the PC for processing. 
 * There are limits, the BMA020 provides various sample rates up to 1200 Hz.  The Arduino i2c speed is limited, as well as the Serial communications has its limits.
 * For the purposes of speed, Serial communications should be in binary form, the COM monitor won't show anything menaningful.  Also we will want to request lesser data from the accelerometer, for example only Z acceleration.
 * 
 * This software does not write anything to the EEPROM.
 * 
 * Communication estimations:
 * 
 * The Arduinos I2c speed is 100kHz. I have not attempted to increase it. Each byte is read or written using 9 cycles (minimum). 
 * Updating a single channel involves transfer of 3 bytes, that is 27 cycles, and at least 10 more cycles (I didn't check) for the service. 
 * So we can update single channel data at 100 kHz / 37 = 2.7 kHz, which is quite tight (slow) even for single channel update.
 * 
 * The Serial transfer to PC is asynchronous. That means, we don't know how much it affects the update frequency. It shouldn't affect it very much though. 
 * At 115200 it will transfer an update quite fast, roughly at 6 kHz.
 * 
 * Having said all that, it is hardly possible to know the resulting sample rate, and likely it will not be fixed. But the performance shouldn't be that bad really. 
 * So we would transfer the timestamp along with each update. A timestamp is an elapsed time in microseconds. On arduino 16Mhz the resolution is 4 microseconds per LSB.
 * 
 * Wires connection:
 * 
 * Vcc_IN   - 5V
 * GND      - GND
 * SCL      - A5
 * SDA      - A4
 * 
 * Serial commands:
 * AINIT resolutionIndex bandwidthIndex
 * AREAD channelFlags
 * 
 */
#include <Wire.h> 
#include "SerialCommand.h"

//address of the accelerometer (may be different on different boards)
#define BMA020_ADDRESS 0x38

// some registers (taken from datasheet Chapter 7. Global memory map)
#define BMA020_RANGE_BW 0X14  // bits 5,6,7 must be kept unchanged
#define BMA020_DATA 0x02
#define BMA020_CTL 0x15

#define SYNC_NEWDATA_FLAG 1


// Chapter "3.1.2 Range" of the datasheet defines 3 various measurement ranges in terms of +/-g: 2, 4, 8
// The narrower the range, the higher precision you would get.
const int resolutionsCount = 3 - 1;

// Chapter "3.1.3 Bandwidth" defines 7 various bandwidths in terms of Hz: 25, 50, 100, 190, 375, 750, 1500
const int bandwidthsCount = 7 - 1;

unsigned long lastAccelerometerReadMicros;
int accelerometerReadDelay = 0;

const char *ERR = "ERR";
const char *OK = "OK";

bool accelerometerInitialized = false;
bool resetElapsedTime = false;
byte accelerometerChannelReadFlags = 0x0;
byte accelerometerSyncMode = 0;

SerialCommand SCmd;


void setup() 
{
    // not all arduinos will handle 115200
    Serial.begin(115200); 

    SCmd.addCommand("AINIT", commandAccelerometerInit );
    SCmd.addCommand("AREAD", commandAccelerometerRead );
    SCmd.addCommand("ADELAY", commandAccelerometerSetDelay );
    SCmd.addCommand("ASYNC", commandAccelerometerSetSyncMode );
    SCmd.addDefaultHandler( commandUnknown );

    Wire.begin(); 
} 


void loop() 
{ 
    SCmd.readSerial();

    if (accelerometerInitialized && accelerometerChannelReadFlags)
        AccelerometerReadAndSend(); 
    
}



void AccelerometerInit(int resolutionIndex, int bandwidthIndex)
{ 
    resolutionIndex = constrain(resolutionIndex, 0, resolutionsCount);
    bandwidthIndex = constrain(bandwidthIndex, 0, bandwidthsCount);
    
    byte temp[1];
    byte temp1;

    // choose bandwidth and range    
    readFrom(BMA020_ADDRESS, BMA020_RANGE_BW,1,temp);
    temp1= (bandwidthIndex & 0x7) | ((resolutionIndex & 0x18) << 3) | (temp[0] & 0xe0);
    writeTo(BMA020_ADDRESS, BMA020_RANGE_BW, temp1);   
    
    // Chapter "3.1.6 Shadow_dis " says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
    // explains that the locking is done if shadow_dis=0. We need this locking for consistent LSB and MSB byte reads.
    readFrom(BMA020_ADDRESS, BMA020_CTL, 1, temp);
    temp1= (temp[0] & 0xf7);
    writeTo(BMA020_ADDRESS,BMA020_CTL, temp1);
    
    accelerometerInitialized = true;
}




void AccelerometerReadAndSend()
{ 
    long elapsedTime;
    unsigned long now;
    byte result[2];
    bool written = false;

    // X
    if (accelerometerChannelReadFlags & 0x1)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA, 2, result);
        if (accelerometerSyncMode == SYNC_NEWDATA_FLAG && !(result[0] & 0x1))
            return;
        written = true;
        Serial.write(result, 2);
    }
    // Y
    if (accelerometerChannelReadFlags & 0x2)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA+2, 2, result);
        if (!written && accelerometerSyncMode == SYNC_NEWDATA_FLAG && !(result[0] & 0x1))
            return;
        written = true;
        Serial.write(result, 2);
    }
    // Z
    if (accelerometerChannelReadFlags & 0x4)
    {
        readFrom(BMA020_ADDRESS, BMA020_DATA+4, 2, result);
        if (!written && accelerometerSyncMode == SYNC_NEWDATA_FLAG && !(result[0] & 0x1))
            return;
        written = true;
        Serial.write(result, 2);
    }
    now = micros();
    if (resetElapsedTime)
    {
        resetElapsedTime = false;
        lastAccelerometerReadMicros = now;
        elapsedTime = 0;
    }
    else
    {
        // this cast deals with possible overflow (aka rollover), elapsedTime should be positive if the "lastAccelerometerReadMicros" is within single overflow range from "now"
        elapsedTime = (long)(now - lastAccelerometerReadMicros);
        lastAccelerometerReadMicros = now;
    }
    Serial.write((byte*) &elapsedTime, 4);

    if (accelerometerReadDelay > 0)
        delayMicroseconds(accelerometerReadDelay);
}

//Writes val to address register on ACC
void writeTo(int DEVICE, byte address, byte val) 
{
    Wire.beginTransmission(DEVICE);   //start transmission to ACC
    Wire.write(address);               //send register address
    Wire.write(val);                   //send value to write
    Wire.endTransmission();           //end trnsmisson
}

// Reads num bytes starting from address register in to buff array. 
// This is also used to read output channels for X,Y,Z. It can read all at once, or individual channels.
// Chapter "7.12.2 acc_x, acc_y, acc_z" says that we should read LSB-bytes first, this will lock possible subsequent MSB channel update until we read MSB.
// Chapter "7.7.6 showdow_dis" explains that the locking is done if shadow_dis=0.
void readFrom(int DEVICE, byte address , int num ,byte *buff)
{
    Wire.beginTransmission(DEVICE);
    Wire.write(address);
    Wire.endTransmission();
 
    Wire.beginTransmission(DEVICE);
    Wire.requestFrom(DEVICE,num); 
 
    int i=0;
    while(Wire.available())
    {
        buff[i] = Wire.read();
        i++;
    }
    Wire.endTransmission();
}


bool decodeIntArgs( int num, int buff[] )
{
    bool err = false;
    for (int i=0; i<num; ++i)
    {
        char *arg = SCmd.next(); 
        if (arg) 
        {
            buff[i] = atoi(arg);
        }
        else
        {
            err = true;
            break;
        }
    }
    return !err;
}

void printArgs(int num, int buff[])
{
    for (int i=0; i<num; ++i)
    {
        if (i > 0)
            Serial.print(", ");
        Serial.print( buff[i] );
    }
    Serial.println();
}

void commandAccelerometerInit()    
{
    int args[4];
    if (decodeIntArgs(2, args))
    {
        Serial.print(OK);
        
        AccelerometerInit( args[0], args[1] );
        delay(20);

        // read back and send following registers
        // resolution and range
        args[0] = 0;
        readFrom(BMA020_ADDRESS, BMA020_RANGE_BW,1, (byte*)args);
        args[1] = accelerometerChannelReadFlags;
        args[2] = accelerometerReadDelay;
        args[3] = accelerometerSyncMode;
        printArgs( 4, args );
    }
    else
    {
        Serial.println(ERR);
    }
}

void commandUnknown()
{
    Serial.println(ERR); 
}


void commandAccelerometerRead()
{
    int args[1];
    if (decodeIntArgs(1, args))
    {
        if (args[0] < 0 || args[0] > 7)
            Serial.println(ERR);
        else
        {
            resetElapsedTime = true;
            accelerometerChannelReadFlags = (byte)args[0];
            Serial.print(OK);
            printArgs( 1, args );
        }
    }
    else
    {
        Serial.println(ERR);
    }
}


void commandAccelerometerSetDelay()
{
    int args[1];
    if (decodeIntArgs(1, args))
    {
        resetElapsedTime = true;
        accelerometerReadDelay = args[0];
        Serial.print(OK);
        printArgs( 1, args );
    }
    else
    {
        Serial.println(ERR);
    }
}

void commandAccelerometerSetSyncMode()
{
    int args[1];
    if (decodeIntArgs(1, args))
    {
        if (args[0] < 0 || args[0] > 1)
            Serial.println(ERR);
        else
        {
            resetElapsedTime = true;
            accelerometerSyncMode = args[0];
            Serial.print(OK);
            printArgs( 1, args );
        }
    }
    else
    {
        Serial.println(ERR);
    }
}

