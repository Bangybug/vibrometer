package grapher;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.SwingUtilities;

import org.jfree.data.xy.XYSeries;

public class ChartUpdater extends Thread 
{
    private DataTransfer dataTransfer;
    private XYSeries xSeries, ySeries, zSeries;
    
    public final EventSource eventSource = new EventSource();
    public static final String EVENT_UPDATE_SAMPLERATE = "samplerate";

    private Object lock = new Object();
    private boolean shouldClose = false;

    private DataOutputStream fileLogger;
    
    private long avgSampleRate;
    
    ChartFftUpdater fftUpdater;
    
    public ChartUpdater(DataTransfer dataTransfer, XYSeries xSeries, XYSeries ySeries, XYSeries zSeries)
    {
        this.dataTransfer = dataTransfer;
        this.xSeries = xSeries;
        this.ySeries = ySeries;
        this.zSeries = zSeries;
    }
    
    int xNumber = 0;   
    
    private long avgElapsedTime = 0;
    private long lastElapsedTime = 0;
    
    public void resetAvgSampleRateCalc()
    {
        avgElapsedTime = 0;
        avgSampleRate = 0;
    }
    
    
    public void run()
    {
        long waitMillis = System.currentTimeMillis() + 1000;
        
        try 
        {
            while (true)
            {
                DataItem nextItem = dataTransfer.dataOutput.take(); 
                while (null != nextItem)
                {
                    synchronized (lock)
                    {
                        if (shouldClose)
                            break;
                        
                        if (null != fileLogger)
                        {
                            try {
                                nextItem.log(fileLogger);
                            } catch (IOException e1) {
                                fileLogger = null;
                                e1.printStackTrace();
                            }                           
                        }

                        if (lastElapsedTime != nextItem.elapsedTime && nextItem.elapsedTime != 0)
                        {
                            if (0 == avgElapsedTime)
                            {
                                avgElapsedTime = nextItem.elapsedTime;
                            }
                            else
                            {
                                avgElapsedTime = (nextItem.elapsedTime + avgElapsedTime) / 2;
                            }
                            lastElapsedTime = nextItem.elapsedTime;
                            
                            long nowMillis = System.currentTimeMillis();
                            if (nowMillis > waitMillis)
                            {
                            	avgSampleRate = (long)1000000.0f / avgElapsedTime;
                                waitMillis = nowMillis + 1000;
                                eventSource.fireEventAsync(EVENT_UPDATE_SAMPLERATE, new Double(1000000.0f / avgElapsedTime) );
                                
                                if (null != fileLogger)
                                {
                                    try {
                                        fileLogger.flush();
                                    } catch (IOException e) {
                                        fileLogger = null;
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        
                        final DataItem myItem = nextItem;
                       
                        externalAddPoint(myItem.value, myItem.channelFlag);
                        
                        nextItem = dataTransfer.dataOutput.isEmpty() ? null : dataTransfer.dataOutput.take();
                    }
                }
                
                //window.repaint();
            }
        } 
        catch (InterruptedException e) 
        {
            dataTransfer.closePort();
            e.printStackTrace();
            return;
        }
        
    }
    
    
    private void addPoint(float value, int pointNumber, int channelCode)
    {
        if (channelCode == DataTransfer.channelCodes[0])
            xSeries.add(pointNumber, value);
        else if (channelCode == DataTransfer.channelCodes[1])
            ySeries.add(pointNumber, value);
        else if (channelCode == DataTransfer.channelCodes[2])
            zSeries.add(pointNumber, value);    	
    }
    
    
    public void externalAddPoint(float value, int channelCode)
    {
    	final int myNumber = xNumber++;
    	
        SwingUtilities.invokeLater( new Runnable() {
            public void run()
            {
            	addPoint(value, myNumber, channelCode);
            } } );
        
        if (avgSampleRate > 0)
        	updateFft(avgSampleRate, value, channelCode);
    }
    
    public void externalSetSampleRate(long sampleRate)
    {
    	avgSampleRate = sampleRate;
    }
    
    public void close()
    {
        synchronized (lock)
        {
            shouldClose = true;
        }
    }
    
    
    public void setChannelLogFile(String filePath)
    {
        synchronized (lock)
        {
            try 
            {
                if (fileLogger != null)
                {
                    fileLogger.close();
                    fileLogger = null;
                }

                if (filePath != null) 
                {
                    fileLogger = new DataOutputStream(new FileOutputStream( filePath ));
                }
            } 
            catch (FileNotFoundException e) 
            {
                fileLogger = null;
                e.printStackTrace();
            } 
            catch (IOException e) 
            {
                fileLogger = null;
                e.printStackTrace();
            }                   
        }
    }
    
 
    public void setFftUpdater(ChartFftUpdater fftUpdater)
    {
    	this.fftUpdater = fftUpdater;
    }
    
    private void updateFft(long avgSampleRate, float value, int channelCode)
    {
    	fftUpdater.addPoint(avgSampleRate, value, channelCode);
    }
    
    
}