package grapher;

import java.util.Iterator;

public class CircularBufferTest 
{
    public static void main(String[] args)
    {
        CircularBuffer buf = new CircularBuffer(3);
        
        
        buf.put((byte)1);
        buf.put((byte)2);
        buf.put((byte)3);
        printBuffer(buf);
        
        buf.reset();
        buf.put((byte)1);
        buf.put((byte)2);
        buf.put((byte)3);
        buf.put((byte)4);
        printBuffer(buf);

        buf.reset();
        buf.put((byte)1);
        buf.put((byte)2);
        buf.put((byte)3);
        buf.put((byte)4);
        buf.put((byte)5);
        buf.put((byte)6);
        printBuffer(buf);


        buf = new CircularBuffer(4);
        buf.put((byte)'a');
        buf.put((byte)'b');
        buf.put((byte)'c');
        buf.put((byte)'d');
        buf.put((byte)'e');
        buf.put((byte)'e');
        printBuffer(buf);
        
        byte[] search = "de".getBytes();
        int i = buf.reverseFindFirst(search);
        System.out.println("Search result :"+i);
        
        if (i != -1)
            System.out.println("Found data: "+buf.copyToStringReverse(i));
        
        
    }
    
    private static void printBuffer(CircularBuffer buf)
    {
        Iterator<Byte> it = buf.reverseIterator();
        System.out.print("Buf: ");
        while (it.hasNext())
        {
            System.out.print( it.next() );
            if (it.hasNext())
                System.out.print(" ");
        }
        System.out.println();
    }
}
