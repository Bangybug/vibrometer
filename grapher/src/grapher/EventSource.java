package grapher;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.SwingUtilities;

public class EventSource 
{
    HashMap<String, List<IEventListener>> listeners = new HashMap<>();
    
    
    public void addListener(String eventType, IEventListener listener)
    {
        List<IEventListener> eventListeners = listeners.get(eventType);
        if (null == eventListeners)
        {
            eventListeners = new ArrayList<>();
            listeners.put(eventType, eventListeners);
        }
        
        if (!eventListeners.contains(listener))
            eventListeners.add(listener);           
    }
    
    
    public void fireEventSync(String eventType, Object arg) throws InvocationTargetException, InterruptedException
    {
        SwingUtilities.invokeAndWait(new Runnable(){
            public void run()
            {
                List<IEventListener> eventListeners = listeners.get(eventType);
                if (null != eventListeners)
                {
                    for (int i=0; i<eventListeners.size(); ++i)
                    {
                        eventListeners.get(i).handleEvent(eventType, arg);
                    }
                }       
            }
        });
    }
    
    public void fireEventAsync(String eventType, Object arg)
    {
        SwingUtilities.invokeLater(new Runnable(){
            public void run()
            {
                List<IEventListener> eventListeners = listeners.get(eventType);
                if (null != eventListeners)
                {
                    for (int i=0; i<eventListeners.size(); ++i)
                    {
                        eventListeners.get(i).handleEvent(eventType, arg);
                    }
                }       
            }
        });
    }
    
    
}
