package grapher;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.ArrayBlockingQueue;

import com.fazecast.jSerialComm.SerialPort;

/**
 * This class contains routines for interfacing Arduinos running my sketch BMA020_Accelerometer_Streamer.ino 
 * https://bitbucket.org/Bangybug/vibrometer
 * @author Dmitry Pryadkin, drpadawan@ya.ru
 */
public class DataTransfer 
{
    public final EventSource eventSource = new EventSource();
    public static final String EVENT_LOCK = "lock";
    public static final String EVENT_UNLOCK = "unlock";
    public static final String EVENT_COMMAND_RESULT = "cmdresult";
    public static final String EVENT_DISCONNECTED = "disconnected";
    public static final String EVENT_INITIALIZED = "init";
    
    public static final String[] resolutions = {"+/- 2g", "+/- 4g", "+/- 8g" };
    
    public static final String[] syncModes = {"None", "Newdata flag"};
    
    private DataOutputStream fileLogger;
    

    /**
     * Typical sensitivity values from the datasheet, may need further calibration.
     */
    public static final float[][] sensitivityPerResolutionAxis = {
            { 256.0f, 256.0f, 256.0f }, // +/- 2g
            { 128.0f, 128.0f, 128.0f }, // +/- 4g
            { 64.0f, 64.0f, 64.0f }     // +/- 8g
    };

    public static final String[] bandwidths = { "25 Hz", "50 Hz", "100 Hz", "190 Hz", "375 Hz", "750 Hz", "1500 Hz" };
    
    public static final int[] channelCodes = { 0x1, 0x2, 0x4 };

    private boolean commandsLocked = false;
    
    // short-duration lock used to deliver port closure signal to the thread
    private Object portOperationLock = new Object();

    // short-duration lock to synchronize command sending and data retrieval
    private Object sendCommandLock = new Object();
    
    private SerialPort port;
    
    private final byte[] errPattern;
    private final byte[] okPattern;
    
    // each data item may have maximum of 3 channels 2 bytes wide each, plus elapsed time micros 4 bytes 
    private final ByteBuffer dataAccumulator = ByteBuffer.allocate(2*3+4);  

    private boolean pendingInit = false;
    private int currentChannelReadingFlags = 0x0;
    private int pendingChannelReadingFlags = 0x0;
    private int currentBandwidthIndex, currentResolutionIndex;
    private int currentSyncIndex, pendingSyncIndex;
    private int currentDelay, pendingDelay;
    
    private boolean commandsEnabled = true;
    
    public final ArrayBlockingQueue<DataItem> dataOutput = new ArrayBlockingQueue<>(1024);
    
    public DataTransfer()
    {
        errPattern = "ERR".getBytes();
        okPattern = "OK".getBytes();        
        
        dataAccumulator.order(ByteOrder.LITTLE_ENDIAN);
    }
    
    
    public void enableCommands(boolean enable)
    {
        this.commandsEnabled = enable;
    }
    
    public boolean isCommandPending()
    {
        return pendingInit || pendingChannelReadingFlags != currentChannelReadingFlags || pendingSyncIndex != currentSyncIndex ||
                pendingDelay != currentDelay;
    }
    
    
    private void executeCommand(String command, String args) throws IllegalStateException
    {
        synchronized(sendCommandLock)
        {
            if (!commandsEnabled || commandsLocked || null == port)
                throw new IllegalStateException();
                
            eventSource.fireEventAsync(EVENT_LOCK, true);
            commandsLocked = true;
            
            byte[] commandBytes = (command + " " + args + "\r").getBytes(); 
            port.writeBytes( commandBytes, commandBytes.length);
        }
    }
    
    
    public void executeInit(int resolutionIndex, int bandwidthIndex)
    {
        if (commandsEnabled)
        {
            pendingInit = true;
            executeCommand("AINIT", String.format("%s %s", resolutionIndex, bandwidthIndex));
        }
    }

    public void executeChannelSelect(int channelFlags)
    {
        if (commandsEnabled && currentChannelReadingFlags != channelFlags)
        {
            pendingChannelReadingFlags = channelFlags;
            executeCommand("AREAD", String.format("%s", channelFlags));
        }
    }

    public void executeSyncMode(int modeIndex)
    {
        if (commandsEnabled && pendingSyncIndex != modeIndex)
        {
            pendingSyncIndex = modeIndex;
            executeCommand("ASYNC", String.format("%s", modeIndex));
        }
    }
    
    public void executeReadDelay(int delay)
    {
        if (commandsEnabled && currentDelay != delay)
        {
            pendingDelay = delay;
            executeCommand("ADELAY", String.format("%s", delay));
        }
    }

    
    /**
     * Creates the thread for data transfer. 
     * Assumes that you don't call it while you already have an open port.
     * @param port
     */
    public void onPortOpened(SerialPort port) throws IllegalStateException
    {
        if (this.port != null)
            throw new IllegalStateException();
        
        commandsLocked = false;
        currentChannelReadingFlags = 0x0;
        pendingChannelReadingFlags = 0x0;
        pendingInit = false;
    
        currentBandwidthIndex = -1;
        currentResolutionIndex = -1;
        pendingDelay = currentDelay = 0;
        pendingSyncIndex = currentSyncIndex = 0;
        
        this.port = port;
        createThread(port);
    }

    public boolean isReadingX() {
        return 0 != (currentChannelReadingFlags & channelCodes[0]);
    }

    public boolean isReadingY() {
        return 0 != (currentChannelReadingFlags & channelCodes[1]);
    }

    public boolean isReadingZ() {
        return 0 != (currentChannelReadingFlags & channelCodes[2]);
    }

    public int getCurrentBandwidthIndex() {
        return currentBandwidthIndex;
    }

    public int getCurrentResolutionIndex() {
        return currentResolutionIndex;
    }
    
    public int getCurrentDelay() {
        return currentDelay;
    }
    
    public int getCurrentSyncModeIndex() {
        return currentSyncIndex;
    }


    public void closePort()
    {
        // request the current thread to end and close the port (if any)
        synchronized (portOperationLock) 
        {
            this.port = null;
        }
    }
    
    private void fireDisconnect()
    {
        DataTransfer.this.port = null;
        try {
            eventSource.fireEventSync(EVENT_DISCONNECTED, true);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }       
    }
    
    
    private void createThread(final SerialPort port)
    {
        // create a new thread that listens for incoming data
        Thread thread = new Thread(){
            public void run() 
            {
                byte[] data = new byte[16384];
                CircularBuffer stack = new CircularBuffer(64);
                long waitMillis = System.currentTimeMillis() + 1000;
                
                while (true) 
                {
                    // port closed by some reason
                    if (!port.isOpen())
                    {
                        fireDisconnect();
                        return;
                    }
                    
                    // fetch data - we have to do it asap, otherwise Arduino's buffer may overflow
                    synchronized (sendCommandLock)
                    {
                        InputStream is = port.getInputStream();
                        try 
                        {
                            int available = Math.min( is.available(), data.length );
                            if (0 != available)
                                available = is.read(data, 0, available);

                            int startFrom = 0; 
                            if (0 != available)
                            {               
                                if (null != fileLogger)
                                {
                                    fileLogger.write(data, 0, available);
                                    long nowMillis = System.currentTimeMillis();
                                    if (nowMillis > waitMillis)
                                    {
                                        waitMillis = nowMillis + 1000;
                                        fileLogger.flush();
                                    }
                                }
                                
                                // once the command is sent, we discard all pending data before we have the response
                                if (isCommandPending())
                                    startFrom = checkPendingCommandResponse(stack, data, available);
    
                                // if channels are configured, we read the data, otherwise discard the unexpected data  
                                if (!isCommandPending() && currentChannelReadingFlags != 0x0)
                                    processData(data, startFrom, available);
                            }
                        } 
                        catch (IOException e1) 
                        {
                            e1.printStackTrace();
                            fireDisconnect();
                        }
                    }

                    // close port requested?
                    synchronized (portOperationLock) 
                    {
                        if (null == DataTransfer.this.port)
                        {
                            port.closePort();
                            fireDisconnect();
                            return;
                        }
                    }
                    
                }
            }
        };
        thread.start();     
    }
    
    
    
    private int checkPendingCommandResponse(CircularBuffer stack, byte[] data, int count)
    {
        // we are looking for patterns "OK<zero-or more>\r" or "ERR<zero or more>\r"
        // for simplicity lets accumulate up to N bytes on a circular stack, and when we see \r, we check the accumulated bytes for the pattern
        
        for (int i=0; i<count; ++i)
        {
            if (data[i] == '\r')
            {
                int responseIndex = stack.reverseFindFirst(okPattern); 
                if (-1 == responseIndex)
                    responseIndex = stack.reverseFindFirst(errPattern);
                if (-1 != responseIndex)
                {
                    String response = new StringBuilder( stack.copyToStringReverse(responseIndex) ).reverse().toString();
                    processCommandResponse(response);
                    stack.reset();
                    dataAccumulator.position(0);
                    return i+2; // skip following \r\n              
                }
                stack.reset();
            }
            else
            {
                stack.put(data[i]);
            }
        }
        return 0;
    }
    
    private void processCommandResponse(String response)
    {
        commandsLocked = false;
        
        eventSource.fireEventAsync(EVENT_UNLOCK, true);
        
        if (pendingInit)
        {
            if (response.startsWith("OK"))
            {
                response = response.substring(2);               
                String[] tokens = response.split(", ");
                int initResponse = Integer.parseInt(tokens[0]);
                            
                currentBandwidthIndex = initResponse & 0x7;             
                currentResolutionIndex = (initResponse & 0x18) >> 3;
                
                if (currentResolutionIndex >= resolutions.length)
                    currentResolutionIndex = -1;
                if (currentBandwidthIndex >= bandwidths.length)
                    currentBandwidthIndex = -1;

                currentChannelReadingFlags = Integer.parseInt(tokens[1]);
                currentDelay = Integer.parseInt(tokens[2]);
                currentSyncIndex = Integer.parseInt(tokens[3]);

                pendingChannelReadingFlags = currentChannelReadingFlags;
                pendingDelay = currentDelay;
                pendingSyncIndex = currentSyncIndex;
                
                eventSource.fireEventAsync(EVENT_INITIALIZED, true);
            }
            eventSource.fireEventAsync(EVENT_COMMAND_RESULT, "AINIT "+response);

            pendingInit = false;
        }
        else if (pendingChannelReadingFlags != currentChannelReadingFlags)
        {
            if (!response.equals("ERR"))
                currentChannelReadingFlags = pendingChannelReadingFlags;
            pendingChannelReadingFlags = currentChannelReadingFlags;
            eventSource.fireEventAsync(EVENT_COMMAND_RESULT, "AREAD "+response);
        }
        else if (pendingDelay != currentDelay)
        {
            if (!response.equals("ERR"))
                currentDelay = pendingDelay;
            pendingDelay = currentDelay;
            eventSource.fireEventAsync(EVENT_COMMAND_RESULT, "ADELAY "+response);
        }
        else if (pendingSyncIndex != currentSyncIndex)
        {
            if (!response.equals("ERR"))
                currentSyncIndex = pendingSyncIndex;
            pendingSyncIndex = currentSyncIndex;
            eventSource.fireEventAsync(EVENT_COMMAND_RESULT, "ASYNC "+response);
        }
            
    }
    
    
    private void processData(byte[] data, int startFrom, int count)
    {
        // put the full data item to the accumulator
        int dataItemSize = 
                (( (currentChannelReadingFlags & 0x1) != 0) ? 2 : 0) +
                (( (currentChannelReadingFlags & 0x2) != 0) ? 2 : 0) +
                (( (currentChannelReadingFlags & 0x4) != 0) ? 2 : 0) +
                4;
        for (int i=startFrom; i<count; ++i)
        {
            if (dataAccumulator.position() == dataItemSize)
            {
                decodeAccumulatedDataItem();
                dataAccumulator.position(0);
            }
            dataAccumulator.put(data[i]);
        }
    }
    
    
    private int get2sComplement10BitInt(int lsb, int msb)
    {
        return (short)(( (lsb & 0xc0) | (msb << 8) )) >> 6;
    }
    
    
    
    private void decodeAccumulatedDataItem()
    {
        dataAccumulator.position(0);

        final DataItem[] channelItems = new DataItem[channelCodes.length];
        
        for (int i=0; i<channelItems.length; ++i)
        {
            if (0 != (currentChannelReadingFlags & channelCodes[i]))
            {
                int value = get2sComplement10BitInt(dataAccumulator.get(), dataAccumulator.get());
                float sens = sensitivityPerResolutionAxis[ currentResolutionIndex ][ i ];
                DataItem channelItem = channelItems[i] = new DataItem();
                channelItem.channelFlag = DataTransfer.channelCodes[i];
                channelItem.value = value / sens;
            }
        }


        long elapsedTime = dataAccumulator.getInt();
        
        try 
        {
            for (int i=0; i<channelItems.length; ++i)
            {
                DataItem channelItem = channelItems[i];
                if (null != channelItem)
                {
                    channelItem.elapsedTime = elapsedTime;
                    dataOutput.put(channelItem);
                }
            }
        }
        catch (InterruptedException e) 
        {       
            e.printStackTrace();
        }
                
        
    }
    
    
    public void setDataLogFile(String filePath)
    {
        synchronized (sendCommandLock)
        {
            try 
            {
                if (fileLogger != null)
                {
                    fileLogger.close();
                    fileLogger = null;
                }

                if (filePath != null) 
                {
                    fileLogger = new DataOutputStream(new FileOutputStream( filePath ));
                }
            } 
            catch (FileNotFoundException e) 
            {
                fileLogger = null;
                e.printStackTrace();
            } 
            catch (IOException e) 
            {
                fileLogger = null;
                e.printStackTrace();
            }       
        }
    }
    
}
