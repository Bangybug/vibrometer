package grapher;

import java.io.DataOutputStream;
import java.io.IOException;

public class DataItem 
{
    public int channelFlag;
    public float value;
    public long elapsedTime;
    
    
    public void log(DataOutputStream out) throws IOException
    {
        out.writeFloat(value);
    }
}
