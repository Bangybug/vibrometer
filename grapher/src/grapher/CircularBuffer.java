package grapher;

import java.util.Iterator;

/**
 * Very simple non-threadsafe circular buffer, it wraps around unlimited times.
 * Provides reverse iterator and reverse search methods.
 * https://bitbucket.org/Bangybug/vibrometer
 * @author Dmitry Pryadkin, drpadawan@ya.ru
 *
 */
public class CircularBuffer 
{
    private final byte[] buffer;
    private boolean isWrapped;
    int head;
    
    public CircularBuffer(int capacity)
    {
        buffer = new byte[capacity];
    }
    
    public void put(byte b)
    {
        if (head == buffer.length)
        {
            isWrapped = true;
            head = 0;
        }
        buffer[head++] = b;
    }
    
    public void reset()
    {
        head = 0;
        isWrapped = false;
    }
    
    
    public Iterator<Byte> reverseIterator()
    {
        return new Iterator<Byte>()
        {
            int currentPos = head - 1;
            boolean passedLeftBound = currentPos == -1;
            
            @Override
            public boolean hasNext() {
                if (isWrapped)
                {
                    return !passedLeftBound || currentPos >= head;
                }
                else
                {
                    return currentPos >= 0;
                }
            }

            @Override
            public Byte next() {
                Byte ret = null;
                if (isWrapped)
                {
                    if (!passedLeftBound)
                    {
                        ret = buffer[currentPos--];
                        if (currentPos == -1)
                        {
                            currentPos = buffer.length-1;
                            passedLeftBound = true;
                        }                           
                    }
                    else
                    {
                        ret = buffer[currentPos--];
                    }
                }
                else
                {
                    ret = buffer[currentPos--];
                }
                return ret;
            }
    
        };
    }
    
    
    public int reverseFindFirst(byte[] needle)
    {
        int ret = 0;
        int matchIndex = needle.length;
        
        Iterator<Byte> it = reverseIterator();
        while (it.hasNext())
        {
            Byte b = it.next();
            
            if (b == needle[matchIndex-1])
                --matchIndex;
            else if (matchIndex < needle.length)
            {
                matchIndex = needle.length;
                if (b == needle[matchIndex-1])
                    --matchIndex;               
            }
            
            if (matchIndex == 0)
                 return ret;
            ++ret;
        }
        
        return -1;      
    }
    
    
    public String copyToStringReverse( int upToReverseIndex )
    {
        StringBuilder sb = new StringBuilder(upToReverseIndex+1);
        
        Iterator<Byte> it = reverseIterator();
        while (it.hasNext())
        {
            sb.append( (char)it.next().byteValue() );
            if (--upToReverseIndex == -1)
                break;
        }
        
        if (upToReverseIndex != -1)
            throw new IllegalStateException();
        
        return sb.toString();       
    }
}
