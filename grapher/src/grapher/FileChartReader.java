package grapher;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FileChartReader  extends Thread
{
	private volatile boolean shouldClose = false;
	private Object lock = new Object();
	
	ChartUpdater chartUpdater; 
	boolean xChannel; 
	boolean yChannel; 
	boolean zChannel; 
	int sampleRate;
	int realTimePercent;
	
	long sleepMillis = 0;
	
	DataInputStream inputStream;
	
	public FileChartReader(
			ChartUpdater chartUpdater, 
			String fileName, 
			boolean xChannel, 
			boolean yChannel, 
			boolean zChannel, 
			int sampleRate, 
			int realTimePercent)
	{
		this.chartUpdater = chartUpdater;  
		this.xChannel = xChannel; 
		this.yChannel = yChannel; 
		this.zChannel = zChannel; 
		this.sampleRate = sampleRate; 
		this.realTimePercent = realTimePercent;
		
		try 
		{
			inputStream = new DataInputStream(new FileInputStream(fileName));
		} 
		catch (FileNotFoundException e) 
		{
		}
		
		sleepMillis = (long)( (float) (1000.0f / sampleRate) * ( 100.0f / (float)realTimePercent) );
		if (sleepMillis < 10)
			sleepMillis = 0;
	}
	
	
    public void close()
    {
        synchronized (lock)
        {
        	shouldClose = true;
        	lock.notify();            
        }
    }
    
    public boolean isClosed()
    {
    	synchronized (lock)
    	{
    		return shouldClose;
    	}
    }
    
    
    
    
    public void run()
    {
    	while (true)
    	{
			if (null == inputStream)
				shouldClose = true;
			else
			{
				try
				{
					if (xChannel)
					{
						float x = inputStream.readFloat();
						chartUpdater.externalAddPoint(x, DataTransfer.channelCodes[0]);
					}
					if (yChannel)
					{
						float y = inputStream.readFloat();
						chartUpdater.externalAddPoint(y, DataTransfer.channelCodes[1]);
					}
					if (zChannel)
					{
						float z = inputStream.readFloat();
						chartUpdater.externalAddPoint(z, DataTransfer.channelCodes[2]);
					}
				}
				catch (IOException e) 
				{
					shouldClose = true;
				}
			}
			 
		    if (shouldClose)
		    {
		    	doClose();
		    	return;
		    }
		    else
		    {
		    	if (sleepMillis > 0)
		    	{
			    	try {
						TimeUnit.MILLISECONDS.sleep(sleepMillis);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
		    	}
		    }
		}
    }
    
    
    private void doClose()
    {
    	if (null != inputStream)
    	{
    		try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    }
    
}
