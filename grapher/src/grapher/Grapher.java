package grapher;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.fazecast.jSerialComm.SerialPort;

/**
 * Main entry point for BMA020 accelerometer Streamer
 * https://bitbucket.org/Bangybug/vibrometer 
 * @author Dmitry Pryadkin, drpadawan@ya.ru
 */
public class Grapher 
{

    /**
     * If you change this, make sure you have the same value in the Arduino.
     * Not all Ardunios may work at 115200, so you may lower it. But we need highest possible rate. 
     */
    private static final int BAUD_RATE = 115200;
    
    private static SerialPort chosenPort;   
    private static DataTransfer dataTransfer = new DataTransfer();
    private static ChartUpdater chartUpdater;
    private static ChartFftUpdater chartFftUpdater;
    private static JFreeChart chart;
    private static JFreeChart fftChart;
    private static FileChartReader fileReader;
    
    public static void main(String[] args) 
    {
        // create and configure the window
        JFrame window = new JFrame();
        window.setTitle("BMA020 Test");
        window.setSize(1024, 768);
        window.setLayout(new BorderLayout());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        

        // create a drop-down box and connect button, then place them at the top of the window
        JComboBox<String> portList = new JComboBox<String>();
        JButton connectButton = new JButton("Connect");
        JPanel topPanel = new JPanel();
        topPanel.add(portList);
        topPanel.add(connectButton);
        window.add(topPanel, BorderLayout.NORTH);

        // populate the drop-down box
        SerialPort[] portNames = SerialPort.getCommPorts();
        for(int i = 0; i < portNames.length; i++)
            portList.addItem(portNames[i].getSystemPortName());
        
        portList.addItem("File");
    

        // this thread has to run whenever the dataTransfer.dataOutput has new items
        // if you don't poll the data, that will block the communications thread
        chartUpdater = createChart();
        chartUpdater.start();
        
        createFftChart(chartUpdater);
        chartFftUpdater.start();

        createTabbedPane(window);

        // populate BMA020 controls
        createBMA020Panel(window);
        createBottomPanel(window);

        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
                dataTransfer.closePort();
                chartUpdater.close();
                chartFftUpdater.close();
                if (null != fileReader)
                	fileReader.close();
            }
        });
        
        
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_DISCONNECTED, new IEventListener() {
            @Override
            public void handleEvent(String eventType, Object arg) {
                connectButton.setEnabled(true);
                portList.setEnabled(true);
                connectButton.setText("Connect");
            }
        });
        
        connectButton.addActionListener(new ActionListener(){
            @Override public void actionPerformed(ActionEvent arg0) {
        		if (null != fileReader)
        			fileReader.close();

            	if(connectButton.getText().equals("Connect")) {
                	if ("File".equals(portList.getSelectedItem()))
                	{
                		FileOpenDialog dlg = new FileOpenDialog(window);
                		if (dlg.isOk)
                		{
                			chartUpdater.externalSetSampleRate(FileOpenDialog.samplingRate);
                			fileReader = new FileChartReader(
                					chartUpdater,
                					FileOpenDialog.fileName,
                					FileOpenDialog.xChannel,
                					FileOpenDialog.yChannel,
                					FileOpenDialog.zChannel,
                					FileOpenDialog.samplingRate,
                					FileOpenDialog.realTimePercent
                					);
                			fileReader.start();
                			connectButton.setText("Disconnect");
                		}
                	}
                	else
                	{
	                    // attempt to connect to the serial port
	                    chosenPort = SerialPort.getCommPort(portList.getSelectedItem().toString());
	                    chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
	                    chosenPort.setBaudRate(BAUD_RATE);
	                    if(chosenPort.openPort()) {
	                        connectButton.setText("Disconnect");
	                        portList.setEnabled(false);
	                        dataTransfer.onPortOpened(chosenPort);
	                        
	                        try {
	                            Thread.sleep(3000);
	                        } catch (InterruptedException e) {
	                            e.printStackTrace();
	                        }
	                        dataTransfer.executeInit(0, 0);
	                    }
                	}
                } else {
                    // disconnect from the serial port
                	if (!"File".equals(portList.getSelectedItem()))
                	{
                		connectButton.setEnabled(false);
                		dataTransfer.closePort();
                	}
                	else
                	{
                		connectButton.setText("Connect");
                	}
                }
            }
        });
        
        // show the window
        window.setVisible(true);
    }
    
    
    private static class XYZPanelCheckboxFields 
    {
        JCheckBox valueX, valueY, valueZ;
    }
    
    private static JPanel createXYZCheckboxPanel(String name, XYZPanelCheckboxFields fields)
    {
        JPanel panel = new JPanel();
        panel.setBorder( new EmptyBorder(15, 0,0,0));
        panel.setAlignmentX( Component.LEFT_ALIGNMENT );
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        
        JLabel label = new JLabel(name);
        panel.add(label);    

        fields.valueX = new JCheckBox("x");
        panel.add(fields.valueX);

        fields.valueY = new JCheckBox("y");
        panel.add(fields.valueY);

        fields.valueZ = new JCheckBox("z");
        panel.add(fields.valueZ);

        return panel;
    }   
    
    
    private static void sendChannelSelectCommand(XYZPanelCheckboxFields channelFields)
    {
        int channelFlags = 0x0;
        if (channelFields.valueX.isSelected())
            channelFlags = channelFlags | DataTransfer.channelCodes[0];
        if (channelFields.valueY.isSelected())
            channelFlags = channelFlags | DataTransfer.channelCodes[1];
        if (channelFields.valueZ.isSelected())
            channelFlags = channelFlags | DataTransfer.channelCodes[2];
        dataTransfer.executeChannelSelect( channelFlags );      
    }
    
    private static void createBMA020Panel(JFrame window)
    {
        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
        rightPanel.setBorder( new EmptyBorder(5, 5, 5, 5));
        
        // resolutions
        JPanel panelResolutions = new JPanel();
        panelResolutions.setLayout(new BoxLayout(panelResolutions, BoxLayout.PAGE_AXIS));    
        panelResolutions.setAlignmentX( Component.LEFT_ALIGNMENT );

        JLabel labelResolution = new JLabel("Resolution:");
        panelResolutions.add(labelResolution);    
        final JComboBox<String> resolutionList = new JComboBox<String>();
        panelResolutions.add(resolutionList);       
        rightPanel.add(panelResolutions);
        
        // bandwidth
        JPanel panelBandwidth = new JPanel();
        panelBandwidth.setLayout(new BoxLayout(panelBandwidth, BoxLayout.PAGE_AXIS));    
        panelBandwidth.setAlignmentX( Component.LEFT_ALIGNMENT );

        JLabel labelBandwidth = new JLabel("Bandwidth:");
        panelBandwidth.add(labelBandwidth);    
        final JComboBox<String> bandwidthList = new JComboBox<String>();
        bandwidthList.setMaximumSize(new Dimension(100, 26));
        panelBandwidth.add(bandwidthList);      
        rightPanel.add(panelBandwidth);
        
        // channels
        final XYZPanelCheckboxFields channelFields = new XYZPanelCheckboxFields();      
        JPanel channelsPanel = createXYZCheckboxPanel("Read channels:", channelFields);
        ActionListener onChannelClick = new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                sendChannelSelectCommand(channelFields);                
            }
        };
        channelFields.valueX.addActionListener(onChannelClick);
        channelFields.valueY.addActionListener(onChannelClick);
        channelFields.valueZ.addActionListener(onChannelClick);
        rightPanel.add(channelsPanel);
        
        // data acquisition sync 
        JPanel panel = new JPanel();
        panel.setBorder( new EmptyBorder(15, 0,0,0));
        panel.setAlignmentX( Component.LEFT_ALIGNMENT );
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));     
        JLabel label = new JLabel("Data sync:");
        panel.add(label);    
        JComboBox<String> syncModes = new JComboBox<String>();
        syncModes.setMaximumSize(new Dimension(100, 26));
        syncModes.setToolTipText("This does not work with BMA020.");
        panel.add(syncModes);
        rightPanel.add(panel);
        
        // explicit sync (microseconds) 
        panel = new JPanel();
        panel.setBorder( new EmptyBorder(15, 0,0,0));
        panel.setAlignmentX( Component.LEFT_ALIGNMENT );
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));     
        label = new JLabel("Explicit sync (uS):");
        JTextField explicitSync = new JTextField();
        explicitSync.setText("0");
        explicitSync.setMaximumSize(new Dimension(160, 24));
        explicitSync.setToolTipText("Explicit delay time between data acquisition in microseconds. Press ENTER to update.");
        panel.add(label);    
        panel.add(explicitSync);        
        rightPanel.add(panel);
        
        
        window.add(rightPanel, BorderLayout.EAST);

        // populate values
        for (int i=0; i<DataTransfer.resolutions.length; ++i)
            resolutionList.addItem( DataTransfer.resolutions[i] );
        for (int i=0; i<DataTransfer.bandwidths.length; ++i)
            bandwidthList.addItem( DataTransfer.bandwidths[i] );
        for (int i=0; i<DataTransfer.syncModes.length; ++i)
            syncModes.addItem(DataTransfer.syncModes[i]);
            
        
        // set sizes        
        panelResolutions.setMaximumSize( panelResolutions.getPreferredSize() );
        channelsPanel.setMaximumSize(channelsPanel.getPreferredSize());
        rightPanel.setMaximumSize( rightPanel.getPreferredSize() );
        
        // attach listeners

        ActionListener initListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dataTransfer.executeInit( 
                        Math.max(0, resolutionList.getSelectedIndex()), 
                        Math.max(0, bandwidthList.getSelectedIndex()) );
            }
        };
        
        resolutionList.addActionListener( initListener );
        bandwidthList.addActionListener( initListener );

        explicitSync.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                int delay = 0;
                try
                {
                    delay = Integer.parseInt(explicitSync.getText());
                    if (delay > 0)
                    {
                        dataTransfer.executeReadDelay(delay);
                        chartUpdater.resetAvgSampleRateCalc();
                    }
                }
                catch (Exception ex)
                {
                }                               
            }
        });  
        
        syncModes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dataTransfer.executeSyncMode(syncModes.getSelectedIndex());
                chartUpdater.resetAvgSampleRateCalc();
            }
        });
        
        
        // The datatransfer does not support sending multiple commands. It needs to wait for the Arduino's response to each command.
        // So we disable command sending UI after sending a single command. It will be enabled after datatransfer has seen the response.
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_LOCK, new IEventListener() {
            @Override
            public void handleEvent(String eventType, Object arg) {
                setPanelEnabled(rightPanel, false);             
            }
        });     
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_UNLOCK, new IEventListener() {
            @Override
            public void handleEvent(String eventType, Object arg) {
                setPanelEnabled(rightPanel, true);
            }
        });     

        dataTransfer.eventSource.addListener(DataTransfer.EVENT_DISCONNECTED, new IEventListener() {
            @Override
            public void handleEvent(String eventType, Object arg) {
                setPanelEnabled(rightPanel, false);
            }
        });
        
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_INITIALIZED, new IEventListener() {
            public void handleEvent(String eventType, Object arg) {
                dataTransfer.enableCommands(false);
                bandwidthList.setSelectedIndex( dataTransfer.getCurrentBandwidthIndex() );
                resolutionList.setSelectedIndex( dataTransfer.getCurrentResolutionIndex() );
                explicitSync.setText( Integer.toString( dataTransfer.getCurrentDelay() ) );
                syncModes.setSelectedItem( dataTransfer.getCurrentSyncModeIndex() );
                channelFields.valueX.setSelected( dataTransfer.isReadingX() );
                channelFields.valueY.setSelected( dataTransfer.isReadingY() );
                channelFields.valueZ.setSelected( dataTransfer.isReadingZ() );
                chartUpdater.resetAvgSampleRateCalc();
                dataTransfer.enableCommands(true);
            }
        });
                

        setPanelEnabled(rightPanel, false);
    }
    
    
    private static void setPanelEnabled(JPanel panel, Boolean isEnabled) 
    {
        panel.setEnabled(isEnabled);

        Component[] components = panel.getComponents();

        for(int i = 0; i < components.length; i++) 
        {
            if(components[i].getClass().getName() == "javax.swing.JPanel")
                setPanelEnabled((JPanel) components[i], isEnabled);

            components[i].setEnabled(isEnabled);
        }
    }
    
    private static String getCurrentDir()
    {
        return System.getProperty("user.dir") + File.separator;         
    }
    
    private static void createLoggingPanel(JPanel parent)
    {
        JPanel bottomPanel = new JPanel();
        GridBagLayout gridLayout = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();

        bottomPanel.setLayout(gridLayout);
        bottomPanel.setBorder( new EmptyBorder(5, 5, 5, 5));
        
        // average sample label
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5,0,0,0);
        c.weightx = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER; //end row
        final JLabel avgSampleRate = new JLabel();
        bottomPanel.add(avgSampleRate, c);
        
        // com port log enablement
        c.gridwidth = GridBagConstraints.RELATIVE;
        c.weightx = 0.3;
        JCheckBox enableCommLogging = new JCheckBox("Enable com port log");
        bottomPanel.add(enableCommLogging, c);      
        
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.weightx = 0.7;
        JTextField commLogPath = new JTextField();
        commLogPath.setText(getCurrentDir()+"comlog.dat");
        commLogPath.setPreferredSize(new Dimension(400, 24));
        bottomPanel.add(commLogPath, c);

        // data log enablement
        c.gridwidth = GridBagConstraints.RELATIVE;
        c.weightx = 0.3;
        JCheckBox enableDataLogging = new JCheckBox("Enable data log");
        bottomPanel.add(enableDataLogging, c);      
        
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.weightx = 0.7;
        JTextField dataLogPath = new JTextField();
        dataLogPath.setText(getCurrentDir()+"data.dat");
        dataLogPath.setPreferredSize(new Dimension(400, 24));
        bottomPanel.add(dataLogPath, c);
        
        // console output 
        c.weightx = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        final JTextArea text = new JTextArea();
        text.setPreferredSize(new Dimension(400, 100));
        text.setMaximumSize(new Dimension(400, 100));
        text.setMinimumSize(new Dimension(400, 100));
        bottomPanel.add(text, c);
        
        dataTransfer.eventSource.addListener(DataTransfer.EVENT_COMMAND_RESULT, new IEventListener() {
            @Override
            public void handleEvent(String eventType, Object arg) {
                text.setText( text.getText() + "\r\n" + arg.toString()  );
            }
        });             
        
        chartUpdater.eventSource.addListener(ChartUpdater.EVENT_UPDATE_SAMPLERATE, new IEventListener() {
            public void handleEvent(String eventType, Object arg) 
            {
                Double  avg = (Double)arg;
                avgSampleRate.setText("Average sampling rate: "+String.format("%1$,.2f", avg)+" Hz");
            }
        });
        
        enableCommLogging.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dataTransfer.setDataLogFile(enableCommLogging.isEnabled() ? commLogPath.getText() : null);
            }
        });

        enableDataLogging.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                chartUpdater.setChannelLogFile(enableDataLogging.isEnabled() ? dataLogPath.getText() : null);
            }
        });

        
        parent.add(bottomPanel);
    }
    
    private static void createBottomPanel(JFrame window)
    {
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.PAGE_AXIS));
        bottomPanel.setBorder( new EmptyBorder(5, 0, 5, 0));

        createLoggingPanel(bottomPanel);
                
        bottomPanel.setPreferredSize(new Dimension(900, 200));      
        window.add(bottomPanel, BorderLayout.SOUTH);
    }
    
  
    private static ChartUpdater createChart()
    {
        // create the line graph
        XYSeries zSeries = new XYSeries("z-channel", false, true);
        XYSeries xSeries = new XYSeries("x-channel", false, true);
        XYSeries ySeries = new XYSeries("y-channel", false, true);
        XYSeriesCollection dataset = new XYSeriesCollection();
        
        // let's 10 seconds on maximum sampling rate is enough for the chart 
        int MAX_SAMPLING_RATE = 1500;
        int MAX_DISPLAYED_POINTS = MAX_SAMPLING_RATE * 10;
        
        xSeries.setMaximumItemCount(MAX_DISPLAYED_POINTS);
        ySeries.setMaximumItemCount(MAX_DISPLAYED_POINTS);
        zSeries.setMaximumItemCount(MAX_DISPLAYED_POINTS);
        
        dataset.addSeries(xSeries);
        dataset.addSeries(ySeries);
        dataset.addSeries(zSeries);
        
        chart = ChartFactory.createXYLineChart("Accelerometer readings", "Samples (points)", "G readings", dataset);        

        // this thread has to run whenever the dataTransfer.dataOutput has new items
        // if you don't poll the data, that will block the communications thread
        return new ChartUpdater(dataTransfer, xSeries, ySeries, zSeries);
    }
    
    private static void createFftChart(ChartUpdater chartUpdater)
    {
    	XYSeries series = new XYSeries("FFT", false, true);
    	XYSeriesCollection dataset = new XYSeriesCollection();
    	dataset.addSeries(series);
    	fftChart = ChartFactory.createXYLineChart("FFT", "Frequency", "Energy", dataset);
    	chartFftUpdater = new ChartFftUpdater( null, null, series );
    	chartUpdater.setFftUpdater(chartFftUpdater);
    }
    
    private static void createTabbedPane(JFrame window)
    {
    	JTabbedPane tabbedPane = new JTabbedPane();
    	window.add(tabbedPane, BorderLayout.CENTER);
    	
    	tabbedPane.addTab("Recording", null, new ChartPanel(chart),"last 10 seconds of recorded data");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
    

        JPanel fftPanel = new JPanel();
        fftPanel.setLayout(new BoxLayout(fftPanel, BoxLayout.PAGE_AXIS));
        fftPanel.setBorder( new EmptyBorder(0, 0, 0, 0));
        fftPanel.setBackground(Color.WHITE);

        JPanel fftSettingsPanel = new JPanel();
        fftSettingsPanel.setLayout(new BoxLayout(fftSettingsPanel, BoxLayout.X_AXIS));
        fftSettingsPanel.setBorder( new EmptyBorder(5, 0, 0, 0));
        fftSettingsPanel.setBackground(Color.WHITE);
        
        JLabel label = new JLabel("Window: ");
        fftSettingsPanel.add(label);    

        JComboBox<String> windowFunc = new JComboBox<String>();
        windowFunc.addItem(ChartFftUpdater.F_RECTANGULAR);
        windowFunc.addItem(ChartFftUpdater.F_TRIANGULAR);
        windowFunc.addItem(ChartFftUpdater.F_LANCZOS);
        windowFunc.addItem(ChartFftUpdater.F_HANN);
        windowFunc.addItem(ChartFftUpdater.F_HAMMING);
        windowFunc.addItem(ChartFftUpdater.F_GAUSS);
        windowFunc.addItem(ChartFftUpdater.F_COSINE);
        windowFunc.addItem(ChartFftUpdater.F_BLACKMAN);
        windowFunc.addItem(ChartFftUpdater.F_BARTLETT);
        windowFunc.addItem(ChartFftUpdater.F_BARTLETT_HANN);
        windowFunc.setMaximumSize(new Dimension(200, 24));
        windowFunc.setAlignmentX(Component.LEFT_ALIGNMENT);
        fftSettingsPanel.add(windowFunc);
        
        label = new JLabel("Size: ");
        fftSettingsPanel.add(label);
        JTextField sizeField = new JTextField();
        sizeField.setText("512");
        sizeField.setMaximumSize(new Dimension(160, 24));
        sizeField.setToolTipText("Size of the window");
        fftSettingsPanel.add(label);    
        fftSettingsPanel.add(sizeField);
       
        
        fftSettingsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        
        fftPanel.add(fftSettingsPanel);
        fftPanel.add(new ChartPanel(fftChart));

		tabbedPane.addTab("FFT Spectrum", null, fftPanel, "fft");
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_2);
		
		windowFunc.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				chartFftUpdater.updateSettings( windowFunc.getSelectedItem().toString(),  0);
			}
		});
		
		sizeField.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void changedUpdate(DocumentEvent e) {
				try {
					int size = Integer.parseInt( sizeField.getText() );
					chartFftUpdater.updateSettings( windowFunc.getSelectedItem().toString(),  size);
				}
				catch (Throwable ex){}
			}
		});
		
    }
    
    

}