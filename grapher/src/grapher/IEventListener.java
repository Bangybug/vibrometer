package grapher;

public interface IEventListener 
{

    void handleEvent(String eventType, Object arg);
    
    
}
