package grapher;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import org.jfree.data.xy.XYSeries;

import ddf.minim.analysis.BartlettHannWindow;
import ddf.minim.analysis.BartlettWindow;
import ddf.minim.analysis.BlackmanWindow;
import ddf.minim.analysis.CosineWindow;
import ddf.minim.analysis.FFT;
import ddf.minim.analysis.GaussWindow;
import ddf.minim.analysis.HammingWindow;
import ddf.minim.analysis.HannWindow;
import ddf.minim.analysis.LanczosWindow;
import ddf.minim.analysis.RectangularWindow;
import ddf.minim.analysis.TriangularWindow;
import ddf.minim.analysis.WindowFunction;

public class ChartFftUpdater extends Thread 
{
	private XYSeries zSeries;
	private volatile boolean shouldClose = false;
	private int windowSize = 512;
	private WindowFunction windowFunction; 
	private String windowFunctionName;
	
	private Object fftCalculationSync = new Object();
	private CountDownLatch fftCalcEndSync;
	
	
    public static final String F_RECTANGULAR = "Rectangular";
    public static final String F_TRIANGULAR = "Triangular";
    public static final String F_LANCZOS = "Lanczos";
    public static final String F_HANN = "Hann";
    public static final String F_HAMMING = "Hamming";
    public static final String F_GAUSS = "Gauss";
    public static final String F_COSINE = "Cosine";
    public static final String F_BLACKMAN = "Blackman";
    public static final String F_BARTLETT = "Bartlett";
    public static final String F_BARTLETT_HANN = "BartlettHann";
	
	private FFT fft;
	private int sampleRate;
	
	private float[] accumulator = new float[0];
	private volatile int accumulatorIndex = 0;
	private int bufferSize = 0;
	
	public ChartFftUpdater(XYSeries xSeries, XYSeries ySeries, XYSeries zSeries)
	{
		this.zSeries = zSeries;
	}
	
	
	public void updateSettings(String windowFunctionStr, int windowSize)
	{
		synchronized (fftCalculationSync)
		{
			if (windowFunctionName != windowFunctionStr)
			{
				if (F_RECTANGULAR.equals(windowFunctionStr))
					windowFunction = new RectangularWindow();
				else if (F_TRIANGULAR.equals(windowFunctionStr))
					windowFunction = new TriangularWindow();
				else if (F_LANCZOS.equals(windowFunctionStr))
					windowFunction = new LanczosWindow();
				else if (F_HANN.equals(windowFunctionStr))
					windowFunction = new HannWindow();
				else if (F_HAMMING.equals(windowFunctionStr))
					windowFunction = new HammingWindow();
				else if (F_GAUSS.equals(windowFunctionStr))
					windowFunction = new GaussWindow();
				else if (F_COSINE.equals(windowFunctionStr))
					windowFunction = new CosineWindow();
				else if (F_BLACKMAN.equals(windowFunctionStr))
					windowFunction = new BlackmanWindow();
				else if (F_BARTLETT.equals(windowFunctionStr))
					windowFunction = new BartlettWindow();
				else if (F_BARTLETT_HANN.equals(windowFunctionStr))
					windowFunction = new BartlettHannWindow();
				
				if (null != fft)
					fft.window(windowFunction);
			}
	
			if (windowSize > 32 & this.windowSize != windowSize)
			{
				this.windowSize = windowSize;
				if (null != fft)
					fft = null;
			}
		}
	}
	
	 public static int nextPowerOf2(final int a)
	 {
	     int b = 1;
	     while (b < a)
	         b = b << 1;
	     return b;
	 }
	
	public void addPoint(long sampleRate, float value, int channelCode)
	{
		if (channelCode != DataTransfer.channelCodes[2])
			return;
		
		if (sampleRate != this.sampleRate)
		{
			if (sampleRate > 0 && 
					Math.abs( (double)(this.sampleRate - sampleRate) / sampleRate ) > 0.05)
			{
				fft = null;
				this.sampleRate = (int)sampleRate;
			}
		}
		
		if (fft == null && this.sampleRate > 0)
		{
			// XXX we want only a second of recording at this sample rate, but we have to extend the time to the closest power of two
			// as required by the FFT implementation
			//			int bufferSize = nextPowerOf2(this.sampleRate);  

			// lets use explicit size
			int bufferSize = nextPowerOf2(windowSize);
			if (accumulator.length != bufferSize)  // the FFT libary will check the accumulator.length, so we cannot do greedy growth
				accumulator = new float[bufferSize];
			
			this.bufferSize = bufferSize;
			accumulatorIndex = 0;

			fft = new FFT( bufferSize, this.sampleRate);
			if (windowFunction != null)
				fft.window(windowFunction);
			fft.logAverages(30, 12);  // counteroctave spans from 32.703 to 61.735 hz, would like 12 tones maybe
		}
		
		if (null != fft)
		{
			accumulator[accumulatorIndex] = value;
			if (++accumulatorIndex == bufferSize)
			{
				fftCalcEndSync = new CountDownLatch(1);
				synchronized (fftCalculationSync)
				{
					fftCalculationSync.notify();
				}
				try 
				{
					fftCalcEndSync.await(3000, TimeUnit.MILLISECONDS);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
					accumulatorIndex = 0;
				}
			}
		}
	}
	
	public void close()
	{
		shouldClose = true;
		synchronized (fftCalculationSync)
		{
			fftCalculationSync.notify();
		}
	}

    public void run()
    {
    	while (true)
    	{
    		if (shouldClose)
    			return;

    		synchronized (fftCalculationSync)
    		{
	    		if (accumulatorIndex > 0 && accumulatorIndex == bufferSize)
	    		{
	    			fft.forward(accumulator);
					accumulatorIndex = 0;
				
					SwingUtilities.invokeLater( new Runnable() {
						public void run()
						{
							zSeries.clear();
							int avgs = fft.avgSize();
							for (int i=0; i<avgs; ++i)
							{
								float freq = fft.getAverageCenterFrequency(i);
								float energy = fft.getAvg(i);
								zSeries.add(freq, energy);
							}
						}
					});
					
					fftCalcEndSync.countDown();
					
	    		}
	    		try {
					fftCalculationSync.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
    		}
    	}
    }
}
