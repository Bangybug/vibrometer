package grapher;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class FileOpenDialog extends JDialog implements ActionListener
{
	private static final long serialVersionUID = 1L;
	
	public boolean isOk = false;
	public static int samplingRate = 1350;
	
	public static String fileName = "f:\\workspaces\\vibrometer\\grapher\\data2.dat"; 
	
	public static boolean xChannel = false;
	public static boolean yChannel = false;
	public static boolean zChannel = true;
	
	public static int realTimePercent = 100;
	
	public FileOpenDialog(JFrame parent)
	{
		super(parent, "Load file", true);
		
		setPreferredSize(new Dimension(600, 400));
		
	    if (parent != null) 
	    {
	    	Dimension parentSize = parent.getSize(); 
	    	Point p = parent.getLocation(); 
	    	setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
	    }
	    
	    JPanel mainPane = new JPanel();
	    
	    mainPane.setBorder( new EmptyBorder(15, 15,0,0));
	    
	    mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.Y_AXIS));
	    
	    // ----------------- 
	    JPanel filePane = new JPanel();
	    filePane.setBorder( new EmptyBorder(15, 0,0,0));
	    filePane.setLayout(new BoxLayout(filePane, BoxLayout.X_AXIS));
	    filePane.setAlignmentX( Component.LEFT_ALIGNMENT );
	    
	    JLabel label = new JLabel("File: ");
	    filePane.add(label);
	    
	    JTextField fileField = new JTextField();
	    fileField.setMaximumSize(new Dimension(300, 24));
	    fileField.setText(fileName);
	    filePane.add(fileField);
	    
	    JButton button = new JButton("Select");
	    button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				final JFileChooser fc = new JFileChooser();
				if (!fileName.isEmpty())
				{
					fc.setCurrentDirectory(new File( fileName ).getParentFile());
				}

				if (fc.showOpenDialog(FileOpenDialog.this) == JFileChooser.APPROVE_OPTION)
				{
					 File file = fc.getSelectedFile();
					 fileName = file.getAbsolutePath();
					 fileField.setText(fileName);
				}
			}
		});
	    filePane.add(button);
	    
	    mainPane.add(filePane);
	    
	    // ------------------
	    	    
	    JPanel samplingPane = new JPanel();
	    samplingPane.setBorder( new EmptyBorder(15, 0,0,0));
	    samplingPane.setAlignmentX( Component.LEFT_ALIGNMENT );
	    samplingPane.setLayout(new BoxLayout(samplingPane, BoxLayout.X_AXIS));
	    
	    label = new JLabel("Sampling rate: ");
	    samplingPane.add(label);
	    
	    JTextField samplingField = new JTextField();
	    samplingField.setMaximumSize(new Dimension(300, 24));
	    samplingField.setText(Integer.toString(samplingRate));
	    samplingPane.add(samplingField);
	    
	    samplingField.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void changedUpdate(DocumentEvent e) {
				try
				{
					samplingRate = Integer.parseInt( samplingField.getText() );
				}
				catch (Throwable ex)
				{
				}
			}
		});
	    mainPane.add(samplingPane);
	    
	    // ------------------
	    	    
	    JPanel timePane = new JPanel();
	    timePane.setBorder( new EmptyBorder(15, 0,0,0));
	    timePane.setAlignmentX( Component.LEFT_ALIGNMENT );
	    timePane.setLayout(new BoxLayout(timePane, BoxLayout.X_AXIS));
	    
	    label = new JLabel("Realtime percent: ");
	    timePane.add(label);
	    
	    JTextField realtimeField = new JTextField();
	    realtimeField.setMaximumSize(new Dimension(300, 24));
	    realtimeField.setText(Integer.toString(realTimePercent));
	    timePane.add(realtimeField);
	    
	    realtimeField.getDocument().addDocumentListener(new DocumentListener() {
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			public void changedUpdate(DocumentEvent e) {
				try
				{
					realTimePercent = Integer.parseInt( realtimeField.getText() );
				}
				catch (Throwable ex)
				{
				}
			}
		});
	    mainPane.add(timePane);
	    
	    // ------------------

	    JPanel channelPane = new JPanel();
	    channelPane.setBorder( new EmptyBorder(15, 0,0,0));
	    channelPane.setAlignmentX( Component.LEFT_ALIGNMENT );
	    channelPane.setLayout(new BoxLayout(channelPane, BoxLayout.X_AXIS));

	    JCheckBox cbX = new JCheckBox("x");
	    channelPane.add( cbX );

	    JCheckBox cbY = new JCheckBox("y");
        channelPane.add( cbY );

        JCheckBox cbZ = new JCheckBox("z");
        channelPane.add( cbZ );
        
        cbX.setSelected(xChannel);
        cbY.setSelected(yChannel);
        cbZ.setSelected(zChannel);
        
        ChangeListener cbListener = new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				xChannel = cbX.isSelected();
				yChannel = cbY.isSelected();
				zChannel = cbZ.isSelected();
			}
		};
		cbX.addChangeListener(cbListener);
		cbY.addChangeListener(cbListener);
		cbZ.addChangeListener(cbListener);
        

	    mainPane.add(channelPane);

        // ------------------


	    
	    
	    getContentPane().add(mainPane, BorderLayout.NORTH);
	    
	    
	    JPanel buttonPane = new JPanel();

	    button = new JButton("Cancel"); 
	    buttonPane.add(button); 
	    button.addActionListener(this);

	    button = new JButton("OK"); 
	    buttonPane.add(button); 
	    button.addActionListener(this);
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	    pack(); 
	    setVisible(true);
	    
	    button.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		 if (((JButton )e.getSource()).getText().equals("OK"))
		 {
			 isOk = samplingRate > 0 &&  new File(fileName).canRead() && (xChannel || yChannel || zChannel) && realTimePercent > 0;
		 }
		 setVisible(false); 
		 dispose(); 
	}
}
