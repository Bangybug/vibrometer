/*
 *  Copyright (c) 2007 - 2008 by Damien Di Fede <ddf@compartmental.net>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as published
 *   by the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details.
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package ddf.minim;



/**
 * <p>
 * The <code>Minim</code> class is the starting point for most everything
 * you will do with this library. There are methods for obtaining objects for playing audio files:
 * AudioSample and AudioPlayer. There are methods for obtaining an AudioRecorder, 
 * which is how you record audio to disk. There are methods for obtaining an AudioInput, 
 * which is how you can monitor the computer's line-in or microphone, depending on what the
 * user has set as the record source. Finally there are methods for obtaining an AudioOutput, 
 * which is how you can play audio generated by your program, typically by connecting classes 
 * found in the ugens package. 
 * </p>
 * <p>
 * Minim keeps references to all of the resources that are 
 * returned from these various methods so that you don't have to worry about closing them.
 * Instead, when your application ends you can simply call the stop method of your Minim instance.
 * Processing users <em>do not</em> need to do this because Minim detects when a PApplet is passed 
 * to the contructor and registers for a notification of application shutdown.
 * </p>
 * <p>
 * Minim requires an Object that can handle two important
 * file system operations so that it doesn't have to worry about details of 
 * the current environment. These two methods are:
 * </p>
 * <pre>
 * String sketchPath( String fileName )
 * InputStream createInput( String fileName )
 * </pre>
 * <p>
 * These are methods that are defined in Processing, which Minim was originally 
 * designed to cleanly interface with. The <code>sketchPath</code> method is 
 * expected to transform a filename into an absolute path and is used when 
 * attempting to create an AudioRecorder. The <code>createInput</code> method 
 * is used when loading files and is expected to take a filename, which is 
 * not necessarily an absolute path, and return an <code>InputStream</code> 
 * that can be used to read the file. For example, in Processing, the <code>createInput</code>
 * method will search in the data folder, the sketch folder, handle URLs, and absolute paths.
 * If you are using Minim outside of Processing, you can handle whatever cases are 
 * appropriate for your project.
 * </p>
 * 
 * @example Basics/PlayAFile
 * 
 * @author Damien Di Fede
 */

public class Minim
{

	/** @invisible
	 * 
	 * Used internally to report error messages. These error messages will
	 * appear in the console area of the PDE if you are running a sketch from
	 * the PDE, otherwise they will appear in the Java Console.
	 * 
	 * @param message
	 *            the error message to report
	 */
	public static void error(String message)
	{
		System.out.println( "=== Minim Error ===" );
		System.out.println( "=== " + message );
		System.out.println();
	}

	/** @invisible
	 * 
	 * Displays a debug message, but only if {@link #debugOn()} has been called.
	 * The message will be displayed in the console area of the PDE, if you are
	 * running your sketch from the PDE. Otherwise, it will be displayed in the
	 * Java Console.
	 * 
	 * @param message
	 *            the message to display
	 * @see #debugOn()
	 */
	public static void debug(String message)
	{
		String[] lines = message.split( "\n" );
		System.out.println( "=== Minim Debug ===" );
		for ( int i = 0; i < lines.length; i++ )
		{
			System.out.println( "=== " + lines[i] );
		}
		System.out.println();
	}

}
