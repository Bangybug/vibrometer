# Vibrometer #

This is my attempt to make a diy vibrometer. I use Arduino Uno and GY-81 board with BMA020 accelerometer.

There is a Java program "Grapher" for PC that connects to the Arduino via Serial communications port at 115200. It is able to sample at 1.3KHz in single channel mode, which is quite good for my purposes. Grapher can record the data in 32-bit float Big-endian format.

I use free Audacity application from https://sourceforge.net/projects/audacity/ to plot a spectrum and analyze the recorded data in frequency domain.

### How do I get set up? ###

* Java program "Grapher"

In the grapher directory you will find the sources and files compile.bat, run.bat and javapath.bat. Modify javapath to adjust to your environment, you will need to install Java. To run the program, call compile.bat and then run.bat.

![grapher.jpg](https://bitbucket.org/repo/Loo9yox/images/1069312916-grapher.jpg)

You will need to connect the Arduino to the PC, then run Grapher, select COM port and click "Connect". Then select the channels and it should plot the data from the Accelerometer that you connect to Arduino.

You can select accelerometer resolution, as well as control the sampling rate by providing non-zero "Explicit sync" value - a delay between the reads.

* Arduino

In the arduino directory you will find a number of sketches. The main sketch is the BMA020_Accelerometer_Streamer. You will need to upload it to your arduino. There is also a libraries folder with SerialCommand library. You can either install that library to your arduino environment or just copy the contents of the SerialCommand folder to the sketch directory.

You will want to modify the BMA020_Accelerometer_Streamer sketch in two cases. If your Arduino/COM port do not work at 115200, then you may lower this value. You will also have to do it in the Grapher / src / Grapher.java file.  The BMA020 accelerometer on GY-81 board may have different i2c address, in that case you will need to set it in the sketch.  If you are not sure, you may run i2cscanner sketch with your board and it will get you the addresses.

* Recording data

At any time you can click "Enable data log" and it will write accelerometer values in the format of 32-bit float big endian format. 

You can use that in your frequency analysis applications such as Audacity. Just import the file as RAW and select Analyze / Plot spectrum. Here is an example of the spectrum I got after attaching the accelerometer to the Hair cutter that vibrates at 50 Hz.

![spectrum.jpg](https://bitbucket.org/repo/Loo9yox/images/951164189-spectrum.jpg)

* Example 

Video

https://www.youtube.com/watch?v=yMDGSlmiv-Y

Have fun.